<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Test</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <style>
            fieldset {
                background-color: #eeeeee;
            }
            legend {
                background-color: gray;
                color: white;
                padding: 5px 10px;
            }
            input {
                margin: 5px;
            }
            .bug{
                color: red;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="row form-group">
                    <div class="col-md-12" id="title">
                        <!-- # -->
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.15/lodash.min.js"></script>
    <script type="text/javascript">

        function dinhduong(){
             var data =
                    [
                        {
                            "id": 1,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 2,
                            "TenDauVao": "Kh\u00f4ng s\u1ee5t c\u00e2n",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "C\u00e2n n\u1eb7ng",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 1
                        },
                        {
                            "id": 2,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 2,
                            "TenDauVao": "T\u0103ng c\u00e2n",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "C\u00e2n n\u1eb7ng",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 1
                        },
                        {
                            "id": 3,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 2,
                            "TenDauVao": "S\u1ee5t \u2264 5% trong1 th\u00e1ng",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "C\u00e2n n\u1eb7ng",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 2
                        },
                        {
                            "id": 4,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 2,
                            "TenDauVao": "S\u1ee5t \u226410% trong 6 th\u00e1ng",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "C\u00e2n n\u1eb7ng",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 2
                        },
                        {
                            "id": 5,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 2,
                            "TenDauVao": "S\u1ee5t > 5% trong 1 th\u00e1ng",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "C\u00e2n n\u1eb7ng",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 3
                        },
                        {
                            "id": 6,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 2,
                            "TenDauVao": "S\u1ee5t >10%trong 6 th\u00e1ng",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "C\u00e2n n\u1eb7ng",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 3
                        },
                        {
                            "id": 7,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 2,
                            "TenDauVao": "S\u1ee5t c\u00e2n trong 2 tu\u1ea7n qua",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "C\u00e2n n\u1eb7ng",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 3
                        },
                        {
                            "id": 8,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 3,
                            "TenDauVao": "Kh\u00f4ng thay \u0111\u1ed5i",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "L\u01b0\u1ee3ng dinh d\u01b0\u1ee1ng",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 1
                        },
                        {
                            "id": 9,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 3,
                            "TenDauVao": "C\u00f3 c\u1ea3i thi\u1ec7n",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "L\u01b0\u1ee3ng dinh d\u01b0\u1ee1ng",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 1
                        },
                        {
                            "id": 10,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 3,
                            "TenDauVao": "Gi\u1ea3m \u0103n r\u00f5 r\u1ec7t",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "L\u01b0\u1ee3ng dinh d\u01b0\u1ee1ng",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 2
                        },
                        {
                            "id": 11,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 3,
                            "TenDauVao": "Gi\u1ea3m \u0103n nghi\u00eam tr\u1ecdng",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "L\u01b0\u1ee3ng dinh d\u01b0\u1ee1ng",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 3
                        },
                        {
                            "id": 12,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 3,
                            "TenDauVao": "Kh\u00f4ng \u0103n",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "L\u01b0\u1ee3ng dinh d\u01b0\u1ee1ng",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 3
                        },
                        {
                            "id": 13,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 23,
                            "TenDauVao": "Kh\u00f4ng c\u00f3",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "Tri\u1ec7u ch\u1ee9ng ti\u00eau h\u00f3a trong 2 tu\u1ea7n qua: bu\u1ed3n n\u00f4n/n\u00f4n, ti\u00eau ch\u1ea3y, ch\u00e1n \u0103n",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 1
                        },
                        {
                            "id": 14,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 23,
                            "TenDauVao": "C\u00f3 c\u1ea3i thi\u1ec7n \u0111\u00e1ng k\u1ec3",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "Tri\u1ec7u ch\u1ee9ng ti\u00eau h\u00f3a trong 2 tu\u1ea7n qua: bu\u1ed3n n\u00f4n/n\u00f4n, ti\u00eau ch\u1ea3y, ch\u00e1n \u0103n",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 1
                        },
                        {
                            "id": 15,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 23,
                            "TenDauVao": "M\u1ed9t ch\u00fat nh\u01b0ng kh\u00f4ng n\u1eb7ng",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "Tri\u1ec7u ch\u1ee9ng ti\u00eau h\u00f3a trong 2 tu\u1ea7n qua: bu\u1ed3n n\u00f4n/n\u00f4n, ti\u00eau ch\u1ea3y, ch\u00e1n \u0103n",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 2
                        },
                        {
                            "id": 16,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 23,
                            "TenDauVao": "Nhi\u1ec1u ho\u1eb7c n\u1eb7ng",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "Tri\u1ec7u ch\u1ee9ng ti\u00eau h\u00f3a trong 2 tu\u1ea7n qua: bu\u1ed3n n\u00f4n/n\u00f4n, ti\u00eau ch\u1ea3y, ch\u00e1n \u0103n",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 3
                        },
                        {
                            "id": 17,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 24,
                            "TenDauVao": "Kh\u00f4ng b\u1ecb h\u1ea1n ch\u1ebf",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "Ho\u1ea1t \u0111\u1ed9ng v\u00e0 ch\u1ee9c n\u0103ng trong 1 th\u00e1ng qua",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 1
                        },
                        {
                            "id": 18,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 24,
                            "TenDauVao": "Gi\u1ea3m m\u1ed9t ch\u00fat nh\u01b0ng kh\u00f4ng n\u1eb7ng",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "Ho\u1ea1t \u0111\u1ed9ng v\u00e0 ch\u1ee9c n\u0103ng trong 1 th\u00e1ng qua",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 2
                        },
                        {
                            "id": 19,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 24,
                            "TenDauVao": "Nhi\u1ec1u ho\u1eb7c n\u1eb7ng",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "Ho\u1ea1t \u0111\u1ed9ng v\u00e0 ch\u1ee9c n\u0103ng trong 1 th\u00e1ng qua",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 3
                        },
                        {
                            "id": 20,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 25,
                            "TenDauVao": "Kh\u00f4ng m\u1ea5t c\u01a1",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "Kh\u1ed1i c\u01a1",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 1
                        },
                        {
                            "id": 21,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 25,
                            "TenDauVao": "\u0110ang c\u1ea3i thi\u1ec7n",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "Kh\u1ed1i c\u01a1",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 1
                        },
                        {
                            "id": 22,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 25,
                            "TenDauVao": "M\u1ea5t kh\u1ed1i c\u01a1 m\u1ee9c \u0111\u1ed9 nh\u1eb9 ho\u1eb7c trung b\u00ecnh",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "Kh\u1ed1i c\u01a1",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 2
                        },
                        {
                            "id": 23,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 25,
                            "TenDauVao": "M\u1ea5t kh\u1ed1i c\u01a1 r\u00f5 r\u1ec7t",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "Kh\u1ed1i c\u01a1",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 3
                        },
                        {
                            "id": 24,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 26,
                            "TenDauVao": "Kh\u00f4ng m\u1ea5t",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "L\u1edbp m\u1ee1 d\u01b0\u1edbi da",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 1
                        },
                        {
                            "id": 25,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 26,
                            "TenDauVao": "\u0110ang c\u1ea3i thi\u1ec7n",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "L\u1edbp m\u1ee1 d\u01b0\u1edbi da",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 1
                        },
                        {
                            "id": 26,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 26,
                            "TenDauVao": "M\u1ea5t l\u1edbp m\u1ee1 d\u01b0\u1edbi da m\u1ee9c \u0111\u1ed9 nh\u1eb9 ho\u1eb7c trung b\u00ecnh",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "L\u1edbp m\u1ee1 d\u01b0\u1edbi da",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 2
                        },
                        {
                            "id": 27,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 26,
                            "TenDauVao": "M\u1ea5t l\u1edbp m\u1ee1 d\u01b0\u1edbi da r\u00f5 r\u1ec7t",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "L\u1edbp m\u1ee1 d\u01b0\u1edbi da",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 3
                        },
                        {
                            "id": 28,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 27,
                            "TenDauVao": "Kh\u00f4ng ph\u00f9, kh\u00f4ng c\u1ed5 ch\u01b0\u1edbng",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "T\u00ecnh tr\u1ea1ng d\u1ecbch",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 1
                        },
                        {
                            "id": 29,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 27,
                            "TenDauVao": "\u0110ang c\u1ea3i thi\u1ec7n",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "T\u00ecnh tr\u1ea1ng d\u1ecbch",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 1
                        },
                        {
                            "id": 30,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 27,
                            "TenDauVao": "Ph\u00f9, c\u1ed5 ch\u01b0\u1edbng m\u1ee9c \u0111\u1ed9 nh\u1eb9 ho\u1eb7c trung b\u00ecnh",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "T\u00ecnh tr\u1ea1ng d\u1ecbch",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 2
                        },
                        {
                            "id": 31,
                            "ID_LoaiPhieu": 24,
                            "ID_LoaiDauVao": 27,
                            "TenDauVao": "Ph\u00f9, c\u1ed5 ch\u01b0\u1edbng m\u1ee9c \u0111\u1ed9 n\u1eb7ng",
                            "TenLoaiPhieu": "Ng\u01b0\u1eddi tr\u01b0\u1edfng th\u00e0nh (>= 18 tu\u1ed5i)",
                            "TenLoaiDauVao": "T\u00ecnh tr\u1ea1ng d\u1ecbch",
                            "TenNhomDauVao": "\u0110\u00c1NH GI\u00c1 DINH D\u01af\u1ee0NG",
                            "GiaTriDauVao": 3
                        }
                    ]
            function luu(){
                var button_save = '<div class="row form-group">\
                    <div class="col-md-12" style="margin-top: 10px;">\
                        <button type="submit" name="submit" id="action" class="btn btn-primary" style="float: left;">Lưu dữ liệu</button>\
                    </div>\
                </div>';
                return button_save;
            }
            function html(){
                var getData = _.groupBy(data,"TenNhomDauVao")
                var div_parent = $('<div>')
                var inputDauVao = [];
                var div_children = $('<div>')
                for(let key in getData){
                    div_parent.append('<h2>'+key+'</h2>');
                    var get_data = _.groupBy(getData[key], "TenLoaiDauVao")
                    for(let key_chil in get_data){
                        div_children.append(
                            '<fieldset>\
                                <legend>'+key_chil+'</legend>'
                                );
                        var fieldset = $('<div>')
                        $.each(get_data[key_chil], function () {
                            var input = $(`<input type="radio" name=${this.ID_LoaiDauVao} id=${this.id} value=${this.id}>`);
                            var div_children= $(`<div style="margin-left: 50px;">`);
                            div_children.append(input).append('<label for="'+this.id+'">'+this.TenDauVao+'</label>');
                            fieldset.append(div_children).append('</div>');
                            inputDauVao = [...inputDauVao,input]
                        });
                        div_children.append(fieldset);
                    }
                }
                return div_parent.append(div_children);
            }
            $.extend(this, {
              "html": html,
              "luu": luu,
            })
        }
        /*---- Vẽ form ----*/
        var dinhduong = new dinhduong();
        var a = $('#title').append(dinhduong.html())
        a.append(dinhduong.luu())
        /*---- Submit ----*/
            // var result_submit = [];
            // $('#action').click( function() {
                // console.log(inputDauVao);
                /*var data = $(":radio"+":checked")
                .map(function() {
                    result_submit = [...result_submit, {ID_LoaiDauVao: this.name, value: this.value}]
                })
                .get()
                .join();
                console.log(result_submit);*/
                /*var result_submit = [];
                for(h=0;h<getArrIDLoaiDauVao.length;h++){
                    var value = $('input[name="'+getArrIDLoaiDauVao[h]+'"]:checked').val();
                    if(value === undefined){
                        $('input[name="'+getArrIDLoaiDauVao[h]+'"]').parent().addClass('bug');
                    }else{
                        $('input[name="'+getArrIDLoaiDauVao[h]+'"]').parent().removeClass('bug');
                        result_submit = [...result_submit, {ID_LoaiDauVao: getArrIDLoaiDauVao[h], value: value}]
                    }
                }*/
                // console.log(result_submit);
            // });
    </script>
    </body>
</html>
